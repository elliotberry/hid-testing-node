const HID = require("node-hid");

const cliSelect = require("cli-select");
const chalk = require("chalk");

function propFilter(arr, key) {
  return [...new Map(arr.map((item) => [item[key], item])).values()];
}

async function getList() {
  const devices = HID.devices();
  const filteredDevices = await propFilter(devices, "vendorId");
  const deviceSel = [];
  for await (d of filteredDevices) {
    deviceSel.push(`${d.manufacturer} // ${d.product} `);
  }
  return { readableList: deviceSel, list: filteredDevices };
}

const openDevice = async (selectedDevice) => {
  try{
    const device = new HID.HID(selectedDevice.path);
    return device;
  } catch (e) {
    try {
      const device = new HID.HID(selectedDevice.vendorId, selectedDevice.productId);
      return device;
    } catch (e) {
      console.log(e);
      return null;
    }
  }
};

async function startStreamin(selectedDevice) {
  console.log(selectedDevice);
  const device = await openDevice(selectedDevice);
  console.log(device)

  device.on("data", function (data) {

    console.log(`[data] ${data}`);
  });

  device.on("error", function (data) {
    console.log(`[error] ${data}`);
  });
}



function doSelect(rList, list) {
  return new Promise((resolve, reject) => {
    cliSelect({
      values: rList,
      valueRenderer: (value, selected) => {
        if (selected) {
          return chalk.underline(value);
        }
  
        return value;
      },
    }).then(function (info) {
      resolve(info)
    });
  });

}

async function main() {
  const { readableList, list } = await getList();

  let deviceInfo = await doSelect(readableList, list);
  startStreamin(list[deviceInfo.id]);
}
main();
